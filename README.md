CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------

The Fixed text email formatter module allows links to be formatted with a fixed email text.

REQUIREMENTS
------------

This module requires no additional contrib modules.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. Configuration is done on a field by field
basis on the Manage display page by selecting the *Email with text field* formatter.

For usability it is recommended to disable the input of a custom link text.
